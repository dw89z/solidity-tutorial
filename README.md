# SOLIDITY TUTORIAL

솔리디티에 대해 알아보는 튜토리얼 저장소입니다.

2018.12.04. 업데이트.


** https://truffleframework.com/tutorials/pet-shop/ 를 참고하여 만들어졌음을 알려드립니다.




[ 환경 세팅 ]

 - Nodejs
 - NPM
 - Git
 - Truffle Framework (sudo npm install -g truffle)
 - Ganache (http://truffleframework.com/ganache에서 다운로드)
 - Visual code (선택사항) (확장프로그램-solidity 설치)
 - Chrome (확장프로그램-MetaMask 설치)





[ 솔리디티란? ]

이더리움 블록체인 네트워크 안에서 실행되는 스마트 계약을 작성하기 위해 만들어진 고급 프로그래밍 언어입니다.

python, JavaScript, JAVA와 같은 다른 고급 언어와 유사한 특징을 가지고 있습니다. 즉 튜링완전하고 배열, 객체, 다형성, 상속과 같은 익숙한 문법 체계를 가지고 있습니다.

자세한 사항은 Git에 올려놓은 소스파일 주석을 참고하시길 바랍니다.





[ 스마트 계약이란? ]

Smart Contracts. 쉽게 말해서 그냥 .java나 .js같은 소스파일을 말하는 겁니다.

솔리디티 스마트 계약은 서버단(EVM. Ethereum Virtual Machine)에서 구동되며, 컴파일 전에는.sol, 컴파일 후에는 .json 확장자를 가집니다.

예. JAVA vs solidity.

    Source.java (인간이 이해할 수 있는 언어) --- compile(javac) --> Source.class (JVM이 이해할 수 있는 바이트코드)
    Source.sol (인간이 이해할 수 있는 언어) --- compile(solc) --> Source.json (EVM이 이해할 수 있는 바이트코드)





[ Truffle Framework란? ]

솔리디티 프로젝트의 디렉토리 구조와 파일들을 명령어 한번으로 세팅해주고(init),

누군가 잘 만들어놓은 프로젝트를 불러오고(unbox),

소스 내에서 이더리움 네트워크와 간편하게 통신하도록 관련 프로퍼티도 제공하는 등 아주 고마운 녀석입니다.

개발부터 메인넷 배포까지 트러플과 함께하세요.


명령어 리스트

 - truffle version : 트러플 버전 확인
 - truffle unbox <프로젝트명> : 트러플 박스에 업로드되어 있는 특정 프로젝트를 현재 디렉토리 위치에 다운로드
 - truffle complie : 현재 디렉토리 내의 솔리디티 프로젝트를 컴파일 (결과물 => build/contracts/*.json)
 - truffle migrate : 현재 디렉토리 내의 컴파일된 스마트 계약을 이더리움 블록체인 네트워크에 배포
 - truffle test : 현재 디렉토리 내의 test 디렉토리에 작성한 스마트 계약을 가지고 테스트 시도





[ Ganache란? ]

이더리움 스마트 계약은 배포부터 연산까지 모든 활동에 대해 수수료를 지불하도록 되어 있습니다.

이때 가니쉬는 로컬에 테스트용 블록체인 네트워크를 열어 100 ETH를 가진 10개의 계정을 제공해줍니다.

가니쉬를 켜놓으시면 수수료 걱정없이 개발 및 테스트할 수 있습니다.


가니쉬 실행 후 상단 내비게이션에서,

LOGS 메뉴 => 로그창 제일 상단에서 호스트, 포트 등 필요한 서버 정보 확인.

ACCOUNT 메뉴 => 제일 상단에서 무작위 단어들로 이루어진 식별 코드 'MNEMONIC' 확인.





[ MetaMask란? ]

크롬 - dApp - 이더리움 블록체인 네트워크 간 상호작용을 할 수 있도록 도와주는 플러그인입니다.





[ dApp이란? ]

솔리디티로 만든 블록체인 웹애플리케이션을 일컫는 말입니다.





[ web3란? ]

JavaScript 소스상에서 이더리움 블록체인 네트워크와 통신할 수 있도록 해주는 라이브러리입니다.

크롬같은 일반적인 브라우저에는 포함되어있지 않아서 외부 라이브러리를 인클루드해야 합니다.





[ 그래서 이제 어쩌면 되나요? ]

 1. 환경 세팅
 2. 가니쉬 실행 및 계정 10개 생성 확인, 가니쉬 서버 정보 확인
 3. 원하는 곳에 프로젝트 디렉토리 생성 및 커맨드라인에서 해당 디렉토리로 이동
 4. 트러플 명령어로 프로젝트 생성 (튜토리얼을 위해 truffle unbox pet-shop 실행)
 5. 튜토리얼 풀어보기 (튜토리얼 답은 Git 저장소에 주석과 함께 푸시했습니다. 별도의 디렉토리에 풀받아서 비교 해보세요.)
 6. 트러플 명령어로 컴파일 및 마이그레이션(=배포)
 7. 가니쉬에서 이더리움 잔액 차감 확인 (블록체인 서버내 배포하는 것도 수수료가 들어감)
 8. 트러플 명령어로 테스트 및 문제 없음 확인
 9. 크롬 MetaMask 실행 및 패스워드, wallet seed에서 가니쉬 MNEMONIC 값, 커스텀 네트워크 세팅에서 가니쉬 서버 정보 입력.
 10. 프론트단 로직 체크 및 npm run dev 명령어로 웹서버 실행 (lite-server모듈이 기본으로 설치되어 있음. package.json의 scripts 키, bs-config.json의 server 키 참고.)
 11. 잘 작동되는지 테스트



